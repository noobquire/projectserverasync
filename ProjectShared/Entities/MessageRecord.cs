﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectShared.Entities
{
    [Serializable]
    public sealed class MessageRecord
    {
        [JsonProperty(PropertyName = "date_recieved")]
        public DateTime DateRecieved { get; set; }
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }
    }
}
