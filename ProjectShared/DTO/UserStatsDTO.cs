﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    [Serializable]
    public class UserStatsDTO
    {
        [JsonProperty(PropertyName = "user_id")]
        public int UserId { get; set; }
        [JsonProperty(PropertyName = "last_project_id")]
        public int LastProjectId { get; set; }
        [JsonProperty(PropertyName = "last_project_tasks_count")]
        public int LastProjectTasksCount { get; set; }
        [JsonProperty(PropertyName = "cancelled_and_unfinished_tasks_count")]
        public int CancelledAndUnfinishedTasksCount { get; set; }
        [JsonProperty(PropertyName = "longest_task_id")]
        public int LongestTaskId { get; set; }
    }
}
