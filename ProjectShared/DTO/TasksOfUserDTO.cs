﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectShared.DTO
{
    public class TasksOfUserDTO
    {
        public int UserId { get; set; }
        /// <summary>
        /// Enumeration of IDs of tasks, where performer is user
        /// </summary>
        public IEnumerable<int> TasksOfUserIds { get; set; }
    }
}
