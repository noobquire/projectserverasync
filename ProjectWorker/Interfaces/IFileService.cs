﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorker.Interfaces
{
    public interface IFileService
    {
        void WriteRecord(string message);
    }
}
