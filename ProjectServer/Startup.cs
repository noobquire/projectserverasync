﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectServer.DAL;
using ProjectServer.Services;
using ProjectServer.Interfaces;
using ProjectServer.Hubs;
using AutoMapper;
using ProjectServer.MappingProfiles;
using ProjectShared.Entities;
using Microsoft.EntityFrameworkCore;

namespace ProjectServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // services
            services.AddTransient(typeof(IUsersService), typeof(UsersService));
            services.AddTransient(typeof(IProjectsService), typeof(ProjectsService));
            services.AddTransient(typeof(ITeamsService), typeof(TeamsService));
            
            services.AddTransient(typeof(IFileService), typeof(FileService));
            services.AddTransient(typeof(ITasksService), typeof(TasksService));

            // data access
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(IRepository<Project>), typeof(DbProjectRepository));
            services.AddScoped(typeof(IRepository<Task>), typeof(DbTaskRepository));
            services.AddScoped(typeof(IRepository<User>), typeof(DbUserRepository));
            services.AddScoped(typeof(IRepository<Team>), typeof(DbTeamRepository));

            // amqp send/recieve services
            services.AddSingleton(typeof(IQueueService), typeof(QueueService));
            services.AddSingleton(typeof(IMessageService), typeof(MessageService));

            // automapper
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            // signalr
            services.AddSignalR();

            // database context
            services.AddDbContext<ProjectServerDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProjectServerDatabase")));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSignalR(routes =>
            {
                routes.MapHub<RequestHub>("/requesthub");
            });
        }
    }
}