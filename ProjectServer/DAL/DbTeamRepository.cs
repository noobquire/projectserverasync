﻿using Microsoft.EntityFrameworkCore;
using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbTeamRepository : IRepository<Team>
    {
        private ProjectServerDbContext _context;
        public DbTeamRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public async System.Threading.Tasks.Task CreateAsync(Team item)
        {
            if (_context.Teams.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            await _context.Teams.AddAsync(item);
        }

        public async System.Threading.Tasks.Task CreateAsync(IEnumerable<Team> items, CancellationToken cancellationToken = default)
        {
            foreach (var item in items)
            {
                await CreateAsync(item);
                if (cancellationToken.IsCancellationRequested) break;
            }
        }
        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _context.Teams.Remove(await GetAsync(id));
        }

        public async Task<Team> GetAsync(int id)
        {
            return (await _context.Teams.ToListAsync()).Single(p => p.Id == id);
        }

        public async Task<IEnumerable<Team>> GetAllAsync()
        {
            return await _context.Teams.ToListAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(Team item)
        {
            var team = await GetAsync(item.Id);
            team = item;
        }
    }
}
