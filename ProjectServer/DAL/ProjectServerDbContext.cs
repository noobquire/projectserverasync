﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectServer.DAL
{
    public class ProjectServerDbContext : DbContext
    {
        public ProjectServerDbContext(DbContextOptions<ProjectServerDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserProject>().HasKey(up => new { up.UserId, up.ProjectId });

            //Seed(modelBuilder);

            // First we identify the model-types by examining the properties in the DbContext class
            // Here, I am assuming that your DbContext class is called "DataContext"
            var modelTypes = typeof(ProjectServerDbContext).GetProperties()
                             .Where(x => x.PropertyType.IsGenericType && x.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>))
                             .Select(x => x.PropertyType.GetGenericArguments().First())
                             .ToList();

            // Feel free to add any other possible types you may have defined your "Id" property with
            // Here I am assuming that only short, int, and bigint would be considered identity
            var identityTypes = new List<Type> { typeof(Int16), typeof(Int32), typeof(Int64) };

            foreach (Type modelType in modelTypes)
            {
                // Find the first property that is named "id" with the types defined in identityTypes collection
                var key = modelType.GetProperties()
                                   .FirstOrDefault(x => x.Name.Equals("Id", StringComparison.CurrentCultureIgnoreCase) && identityTypes.Contains(x.PropertyType));

                // Once we know a matching property is found
                // We set the propery as Identity using UseSqlServerIdentityColumn() method
                if (key == null)
                {
                    continue;
                }

                // Here we identify the Id property to be set to Identity
                // Also, we use change the PropertySaveBehavior on the same
                // property to ignore the values 
                modelBuilder.Entity(modelType)
                            .Property(key.Name)
                            .UseSqlServerIdentityColumn()
                            .Metadata.BeforeSaveBehavior = PropertySaveBehavior.Ignore;

                base.OnModelCreating(modelBuilder);
            }
        }

        private void Seed(ModelBuilder modelBuilder)
        {
            #region Creating entities
            var project1 = new Project
            {
                Id = 1,
                Name = "First Project",
                Description = "We are doing some real stuff here",
                StartedAt = new DateTime(2017, 04, 12),
                Deadline = new DateTime(2021, 05, 18),
            };
            var project2 = new Project
            {
                Id = 2,
                Name = "Project number two",
                Description = "Let's have some fun!",
                StartedAt = new DateTime(2018, 09, 01),
                Deadline = new DateTime(2019, 06, 27),
            };
            var project3 = new Project
            {
                Id = 3,
                Name = "This is our third project",
                Description = "Together we can change the world, just one random act of kindness at a time.",
                StartedAt = new DateTime(2016, 03, 05),
                Deadline = new DateTime(2017, 05, 01),
            };
            var project4 = new Project
            {
                Id = 4,
                Name = "Fourth and biggest project to this day",
                Description = "Someone that you obviously can trust and someone that you would trust your life with - that's a really good friend.",
                StartedAt = new DateTime(2013, 09, 06),
                Deadline = new DateTime(2023, 01, 06),
            };

            var user1 = new User
            {
                Id = 1,
                FirstName = "John",
                LastName = "Doe",
                Email = "johndoe@gmail.com",
                Birthday = new DateTime(1996, 07, 19),
                RegisteredAt = new DateTime(2015, 03, 07),
            };
            var user2 = new User
            {
                Id = 2,
                FirstName = "Vasya",
                LastName = "Poopkin",
                Email = "vpoopkin@gmail.com",
                Birthday = new DateTime(1998, 01, 27),
                RegisteredAt = new DateTime(2015, 05, 13),
            };
            var user3 = new User
            {
                Id = 3,
                FirstName = "Ilya",
                LastName = "Muromets",
                Email = "imurms@gmail.com",
                Birthday = new DateTime(1990, 03, 20),
                RegisteredAt = new DateTime(2015, 05, 13),
            };
            var user4 = new User
            {
                Id = 4,
                FirstName = "Alexey",
                LastName = "Popovich",
                Email = "appvich@gmail.com",
                Birthday = new DateTime(1995, 09, 21),
                RegisteredAt = new DateTime(2016, 07, 03),
            };
            var user5 = new User
            {
                Id = 5,
                FirstName = "Lev",
                LastName = "Tolstoy",
                Email = "ltolstoy@gmail.com",
                Birthday = new DateTime(1989, 04, 11),
                RegisteredAt = new DateTime(2016, 08, 13),
            };

            var team1 = new Team
            {
                Id = 1,
                Name = "Team Green",
                CreatedAt = new DateTime(2015, 03, 07),
            };
            var team2 = new Team
            {
                Id = 2,
                Name = "Team Grey",
                CreatedAt = new DateTime(2016, 02, 07),
            };

            var userProject1 = new UserProject
            {
                UserId = 1,
                User = user1,
                ProjectId = 2,
                Project = project2,
            };
            var userProject2 = new UserProject
            {
                UserId = 2,
                User = user2,
                ProjectId = 1,
                Project = project1,
            };
            var userProject3 = new UserProject
            {
                UserId = 2,
                User = user2,
                ProjectId = 3,
                Project = project3,
            };
            var userProject4 = new UserProject
            {
                UserId = 2,
                User = user2,
                ProjectId = 4,
                Project = project4,
            };
            var userProject5 = new UserProject
            {
                UserId = 3,
                User = user3,
                ProjectId = 2,
                Project = project2,
            };
            var userProject6 = new UserProject
            {
                UserId = 4,
                User = user4,
                ProjectId = 1,
                Project = project1,
            };
            var userProject7 = new UserProject
            {
                UserId = 4,
                User = user4,
                ProjectId = 3,
                Project = project3,
            };
            var userProject8 = new UserProject
            {
                UserId = 4,
                User = user4,
                ProjectId = 4,
                Project = project4,
            };
            var userProject9 = new UserProject
            {
                UserId = 5,
                User = user5,
                ProjectId = 2,
                Project = project2,
            };


            var task1 = new Task
            {
                Id = 1,
                Name = "Consectetur adipiscing elit, sed do eiusmod tempor",
                Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                CreatedAt = new DateTime(2017, 12, 06),
                CompletedAt = new DateTime(2017, 06, 21),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task2 = new Task
            {
                Id = 2,
                Name = "In reprehenderit consectetur dolor in voluptate velit esse",
                Description = "Gute irure dolor in reprehenderit in voluptate velit aute irure dolor in reprehenderit.",
                CreatedAt = new DateTime(2017, 05, 12),
                CompletedAt = new DateTime(2017, 06, 07),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task3 = new Task
            {
                Id = 3,
                Name = "Donec vitae augue ex",
                Description = "Sed dapibus mi rhoncus purus tristique, dignissim efficitur est tristique.",
                CreatedAt = new DateTime(2018, 03, 19),
                CompletedAt = new DateTime(2018, 04, 15),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task4 = new Task
            {
                Id = 4,
                Name = "Aliquam erat volutpat",
                Description = "Nunc eu cursus libero, nec tempus mi. Etiam gravida lorem orci, et faucibus lorem mollis quis. Sed hendrerit arcu nisl, cursus pulvinar tellus luctus vitae.",
                CreatedAt = new DateTime(2018, 03, 17),
                CompletedAt = new DateTime(2018, 06, 14),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task5 = new Task
            {
                Id = 5,
                Name = "Vivamus purus ante, venenatis id",
                Description = "Maecenas lacinia facilisis mauris, eget feugiat arcu ornare quis. Nulla nisi lectus, eleifend id eros sit amet, mollis ultrices erat. Etiam vel aliquet libero.",
                CreatedAt = new DateTime(2019, 01, 19),
                CompletedAt = new DateTime(2019, 03, 09),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task6 = new Task
            {
                Id = 6,
                Name = "Ut ut tincidunt sapien, sed convallis mi.",
                Description = "Donec tristique luctus elit sit amet luctus. Pellentesque lacus felis, pulvinar nec libero a, egestas semper justo.",
                CreatedAt = new DateTime(2019, 04, 14),
                CompletedAt = new DateTime(2019, 07, 31),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            var task7 = new Task
            {
                Id = 7,
                Name = "Nunc eu cursus libero, nec tempus mi",
                Description = "Etiam gravida lorem orci, et faucibus lorem mollis quis. Sed hendrerit arcu nisl, cursus pulvinar tellus luctus vitae. Integer at ultricies ante.",
                CreatedAt = new DateTime(2019, 07, 11),
                CompletedAt = new DateTime(2019, 09, 21),
                State = ProjectShared.DTO.TaskState.Finished,
            };
            #endregion

            var projects = new[] { project1, project2, project3, project4 };
            var users = new[] { user1, user2, user3, user4, user5 };
            var tasks = new[] { task1, task2, task3, task4, task5, task6, task7 };
            var teams = new[] { team1, team2 };

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);


            /*
             * 
             * 
            #region Adding entities to database



            modelBuilder.Entity<Project>(p =>
            {
                p.HasData(project1);
                p.OwnsOne(e => e.Team).HasData(team2);
                //p.OwnsOne(e => e.Users).HasData(new[] { userProject1, userProject6 });
                p.OwnsOne(e => e.Author).HasData(user1);
            });
            modelBuilder.Entity<Project>(p =>
            {
                p.HasData(project2);
                p.OwnsOne(e => e.Team).HasData(team1);
                //p.OwnsOne(e => e.Users).HasData(new[] { userProject2, userProject5, userProject9 });
                p.OwnsOne(e => e.Author).HasData(user2);
            });
            modelBuilder.Entity<Project>(p =>
            {
                p.HasData(project3);
                p.OwnsOne(e => e.Team).HasData(team2);
                // p.OwnsOne(e => e.Users).HasData(new[] { userProject3, userProject7 });
                p.OwnsOne(e => e.Author).HasData(user1);
            });
            modelBuilder.Entity<Project>(p =>
            {
                p.HasData(project4);
                p.OwnsOne(e => e.Team).HasData(team2);
                //p.OwnsOne(e => e.Users).HasData(new[] { userProject4, userProject8 });
                p.OwnsOne(e => e.Author).HasData(user5);
            });
            modelBuilder.Entity<User>(u =>
            {
                u.HasData(user1);
                u.OwnsOne(e => e.Team).HasData(team1);
                // u.OwnsOne(e => e.Projects).HasData(new[] { userProject1 });
            });
            modelBuilder.Entity<User>(u =>
            {
                u.HasData(user2);
                u.OwnsOne(e => e.Team).HasData(team2);
                // u.OwnsOne(e => e.Projects).HasData(new[] { userProject2, userProject3, userProject4 });
            });
            modelBuilder.Entity<User>(u =>
            {
                u.HasData(user3);
                u.OwnsOne(e => e.Team).HasData(team1);
                // u.OwnsOne(e => e.Projects).HasData(new[] { userProject5 });
            });
            modelBuilder.Entity<User>(u =>
            {
                u.HasData(user4);
                u.OwnsOne(e => e.Team).HasData(team2);
                // u.OwnsOne(e => e.Projects).HasData(new[] { userProject6, userProject7, userProject8 });
            });
            modelBuilder.Entity<User>(u =>
            {
                u.HasData(user5);
                u.OwnsOne(e => e.Team).HasData(team1);
                // u.OwnsOne(e => e.Projects).HasData(new[] { userProject9 });
            });
            /*
            modelBuilder.Entity<Team>(u =>
            {
                u.HasData(team1);
                u.OwnsOne(e => e.Users).HasData(new[] { user2, user4 });
            });
            /*
            modelBuilder.Entity<Team>(u =>
            {
                u.HasData(team2);
                u.OwnsOne(e => e.Users).HasData(new[] { user1, user3, user5 });
            });
            
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task1);
                u.OwnsOne(e => e.Project).HasData(project1);
                u.OwnsOne(e => e.Performer).HasData(user2);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task2);
                u.OwnsOne(e => e.Project).HasData(project1);
                u.OwnsOne(e => e.Performer).HasData(user1);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task3);
                u.OwnsOne(e => e.Project).HasData(project3);
                u.OwnsOne(e => e.Performer).HasData(user3);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task4);
                u.OwnsOne(e => e.Project).HasData(project1);
                u.OwnsOne(e => e.Performer).HasData(user2);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task5);
                u.OwnsOne(e => e.Project).HasData(project4);
                u.OwnsOne(e => e.Performer).HasData(user5);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task6);
                u.OwnsOne(e => e.Project).HasData(project2);
                u.OwnsOne(e => e.Performer).HasData(user1);
            });
            modelBuilder.Entity<Task>(u =>
            {
                u.HasData(task7);
                u.OwnsOne(e => e.Project).HasData(project2);
                u.OwnsOne(e => e.Performer).HasData(user1);
            });
            */
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
