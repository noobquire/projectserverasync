﻿using ProjectServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using ProjectShared.DTO;

namespace ProjectServer.DAL
{
    public class DbTaskRepository : IRepository<Task>
    {
        private ProjectServerDbContext _context;
        public DbTaskRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public async System.Threading.Tasks.Task CreateAsync(Task item)
        {
            if (_context.Tasks.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            await _context.Tasks.AddAsync(item);
        }

        public async System.Threading.Tasks.Task CreateAsync(IEnumerable<Task> items, CancellationToken cancellationToken = default)
        {
            foreach (var item in items)
            {
                await CreateAsync(item);
                if (cancellationToken.IsCancellationRequested) break;
            }
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _context.Tasks.Remove(await GetAsync(id));
        }

        public async System.Threading.Tasks.Task<Task> GetAsync(int id)
        {
            var tasks = await _context.Tasks.ToListAsync();
            return tasks.Single(t => t.Id == id);
        }

        public async System.Threading.Tasks.Task<IEnumerable<Task>> GetAllAsync()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async System.Threading.Tasks.Task UpdateAsync(Task item)
        {
            var task = await GetAsync(item.Id);
            task = item;
        }
    }
}
