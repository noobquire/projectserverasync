﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectServer.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FinishedAt",
                table: "Tasks",
                newName: "CompletedAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CompletedAt",
                table: "Tasks",
                newName: "FinishedAt");
        }
    }
}
