﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(int id);
        Task CreateAsync(T item);
        Task CreateAsync(IEnumerable<T> items, CancellationToken cancellationToken = default);
        Task UpdateAsync(T item);
        Task DeleteAsync(int id);
    }
}