﻿using ProjectShared.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface IUsersService : ICrudService<UserDTO>
    {
        /// <summary>
        /// Get count of tasks per each project which was created by this user
        /// </summary>
        /// <returns>Key: project ID, value: amount of tasks</returns>
        Task<IDictionary<int, int>> GetTaskCountAsync(int userId);
        Task<IEnumerable<TaskDTO>> GetTasksWithNameLengthLessThan45Async(int userId);
        Task<IEnumerable<TaskDTO>> GetTasksFinishedIn2019Async(int userId);
        Task<UserStatsDTO> GetUserStatsAsync(int userId);
        Task<IEnumerable<TasksOfUserDTO>> GetUsersSortedByFirstNameWithTasksSortedByNameLengthAsync();
    }
}
