﻿using RabbitMQ.Client.Events;
using System;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface IMessageService : IDisposable
    {
        void Configure();
        void MessageRecieved(object sender, BasicDeliverEventArgs e);
    }
}
