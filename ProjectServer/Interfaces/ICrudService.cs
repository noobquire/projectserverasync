﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface ICrudService<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(int id);
        Task CreateAsync(T item);
        Task CreateManyAsync(IEnumerable<T> items);
        Task UpdateAsync(T item);
        Task DeleteAsync(int id);
    }
}
