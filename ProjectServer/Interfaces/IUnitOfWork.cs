﻿using ProjectShared.Entities;

namespace ProjectServer.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<Task> Tasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
        void SaveChanges();
        System.Threading.Tasks.Task SaveChangesAsync();
    }
}
