using ProjectShared.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface ITeamsService : ICrudService<TeamDTO>
    {
        Task<IEnumerable<UsersInTeamDTO>> GetTeamsWithUsersOlderThan12Async();
    }
}
