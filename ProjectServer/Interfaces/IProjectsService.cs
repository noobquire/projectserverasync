﻿using ProjectShared.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface IProjectsService : ICrudService<ProjectDTO>
    {
        Task<ProjectStatsDTO> GetProjectStats(int projectId);
    }
}
