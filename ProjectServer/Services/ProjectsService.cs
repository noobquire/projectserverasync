﻿using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectShared.Entities;
using System;
using System.Threading.Tasks;

namespace ProjectServer.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task CreateAsync(ProjectDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            await _unitOfWork.Projects.CreateAsync(_mapper.Map<Project>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task CreateManyAsync(IEnumerable<ProjectDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            await _unitOfWork.Projects.CreateAsync(_mapper.Map<Project[]>(items));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            await _unitOfWork.Projects.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<ProjectDTO> GetAsync(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<ProjectDTO>(await _unitOfWork.Projects.GetAsync(id));
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<ProjectDTO[]>(await _unitOfWork.Projects.GetAllAsync());
        }

        public async Task<ProjectStatsDTO> GetProjectStats(int projectId)
        {
            _queueService.PostValue("Getting project stats was triggered");
            return (from project in await _unitOfWork.Projects.GetAllAsync()
                    join task in await _unitOfWork.Tasks.GetAllAsync() on project.Id equals task.Project.Id into projectTasks
                    join user in await _unitOfWork.Users.GetAllAsync() on project.Team.Id equals user.Team.Id into projectTeam
                    select new ProjectStatsDTO
                    {
                        ProjectId = project.Id,

                        TaskWithLongestDescId = projectTasks.Any() ? projectTasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault().Id : -1,

                        TaskWithShortestNameId = projectTasks.Any() ? projectTasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault().Id : -1,

                        TotalUsersInProjectTeam =
                        (project.Description.Length > 25 || projectTasks.Count() < 3) ?
                        projectTeam.Count() : 0,
                    }).FirstOrDefault(ps => ps.ProjectId == projectId);

        }

        public async System.Threading.Tasks.Task UpdateAsync(ProjectDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            await _unitOfWork.Projects.UpdateAsync(_mapper.Map<Project>(item));
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
