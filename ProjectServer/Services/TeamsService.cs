﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectServer.DAL;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using AutoMapper;
using ProjectShared.Entities;

namespace ProjectServer.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task CreateAsync(TeamDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            await _unitOfWork.Teams.CreateAsync(_mapper.Map<Team>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task CreateManyAsync(IEnumerable<TeamDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            await _unitOfWork.Teams.CreateAsync(_mapper.Map<Team[]>(items));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            await _unitOfWork.Teams.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<TeamDTO> GetAsync(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<TeamDTO>(await _unitOfWork.Teams.GetAsync(id));
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<TeamDTO[]>(await _unitOfWork.Teams.GetAllAsync());
        }

        public async System.Threading.Tasks.Task UpdateAsync(TeamDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            await _unitOfWork.Teams.UpdateAsync(_mapper.Map<Team>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<UsersInTeamDTO>> GetTeamsWithUsersOlderThan12Async()
        {
            _queueService.PostValue("Getting teams with users older than 12 was triggered");
            return
                (await _unitOfWork.Users.GetAllAsync())
                .Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                .OrderByDescending(u => DateTime.Now.Ticks - u.RegisteredAt.Ticks)
                .GroupBy(u => u.Team.Id)
                .Select(async g => new UsersInTeamDTO
                {
                    TeamId = g.Key,
                    TeamName = (await _unitOfWork.Teams.GetAsync(g.Key)).Name,
                    UsersInTeamIds = g.Select(u => u.Id),
                })
                .Select(t => t.Result);
        }
    }
}
