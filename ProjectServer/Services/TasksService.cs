﻿using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectShared.Entities;
using System;
using System.Threading.Tasks;

namespace ProjectServer.Services
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public TasksService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task CreateAsync(TaskDTO item)
        {
            _queueService.PostValue("Creating a tassk was triggered");
            await _unitOfWork.Tasks.CreateAsync(_mapper.Map<ProjectShared.Entities.Task>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task CreateManyAsync(IEnumerable<TaskDTO> items)
        {
            _queueService.PostValue("Creating many tasks was triggered");
            await _unitOfWork.Tasks.CreateAsync(_mapper.Map<ProjectShared.Entities.Task[]>(items));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _queueService.PostValue("Deleting a task was triggered");
            await _unitOfWork.Tasks.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<TaskDTO> GetAsync(int id)
        {
            _queueService.PostValue("Getting a task by id was triggered");
            return _mapper.Map<TaskDTO>(await _unitOfWork.Tasks.GetAsync(id));
        }

        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            _queueService.PostValue("Getting all tasks was triggered");
            return _mapper.Map<TaskDTO[]>(await _unitOfWork.Tasks.GetAllAsync());
        }

        public async System.Threading.Tasks.Task UpdateAsync(TaskDTO item)
        {
            _queueService.PostValue("Updating a task was triggered");
            await _unitOfWork.Tasks.UpdateAsync(_mapper.Map<ProjectShared.Entities.Task>(item));
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
