﻿using System.Collections.Generic;
using System.Linq;
using ProjectServer.DAL;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using AutoMapper;
using ProjectShared.Entities;
using System.Threading.Tasks;

namespace ProjectServer.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _queueService = queueService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async System.Threading.Tasks.Task CreateAsync(UserDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            await _unitOfWork.Users.CreateAsync(_mapper.Map<User>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task CreateManyAsync(IEnumerable<UserDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            await _unitOfWork.Users.CreateAsync(_mapper.Map<User[]>(items));
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            await _unitOfWork.Users.DeleteAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<UserDTO> GetAsync(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<UserDTO>(await _unitOfWork.Users.GetAsync(id));
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<UserDTO[]>(await _unitOfWork.Users.GetAllAsync());
        }

        public async System.Threading.Tasks.Task UpdateAsync(UserDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            await _unitOfWork.Users.UpdateAsync(_mapper.Map<User>(item));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IDictionary<int, int>> GetTaskCountAsync(int userId)
        {
            _queueService.PostValue("Getting count of tasks per each project of user was triggered");
            return (await _unitOfWork.Projects.GetAllAsync())
                .AsParallel()
                .Where(p => p.Author.Id == userId)
                .ToDictionary(p => p.Id, async p => (await _unitOfWork.Tasks.GetAllAsync())
                .Where(t => t.Project.Id == p.Id).Count())
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Result);

        }

        public async Task<IEnumerable<TaskDTO>> GetTasksFinishedIn2019Async(int userId)
        {
            _queueService.PostValue("Getting tasks of a user finished in 2019 was triggered");
            return _mapper.Map<TaskDTO[]>((await _unitOfWork.Tasks.GetAllAsync()).Where(t => t.Performer.Id == userId
                && t.State == TaskState.Finished
                && t.CompletedAt.Year == 2019));
        }

        public async Task<IEnumerable<TasksOfUserDTO>> GetUsersSortedByFirstNameWithTasksSortedByNameLengthAsync()
        {
            _queueService.PostValue("Getting users sorted by first name with their tasks sorted by name length was triggered");
            return (await _unitOfWork.Users.GetAllAsync())
                .OrderBy(u => u.FirstName)
                .Select(async u => new TasksOfUserDTO
                {
                    UserId = u.Id,
                    TasksOfUserIds = (await _unitOfWork.Tasks.GetAllAsync())
                    .Where(t => t.Performer.Id == u.Id)
                    .OrderByDescending(t => t.Name.Length)
                    .Select(t => t.Id),
                })
                .Select(t => t.Result);
        }

        public async Task<UserStatsDTO> GetUserStatsAsync(int userId)
        {
            _queueService.PostValue("Getting user stats was triggered");
            return (await _unitOfWork.Projects.GetAllAsync())
                .Where(p => p.Author.Id == userId)
                .OrderByDescending(p => p.StartedAt)
                .Select(async p => new UserStatsDTO
                {
                    UserId = userId,
                    LastProjectId = p.Id,
                    LastProjectTasksCount = (await _unitOfWork.Tasks.GetAllAsync())
                    .Where(t => t.Project.Id == p.Id).Count(),
                    CancelledAndUnfinishedTasksCount = (await _unitOfWork.Tasks.GetAllAsync())
                    .Where(t => t.Project.Id == p.Id && t.State != TaskState.Finished).Count(),
                    LongestTaskId = (await _unitOfWork.Tasks.GetAllAsync())
                    .Where(t => t.Project.Id == p.Id)
                    .OrderBy(t => t.CreatedAt)
                    .ThenByDescending(t => t.CompletedAt)
                    .FirstOrDefault().Id,
                })
                .Select(t => t.Result)
                .FirstOrDefault();
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksWithNameLengthLessThan45Async(int userId)
        {
            _queueService.PostValue("Getting of a user tasks with name length less than 45 symbols was triggered");
            return _mapper.Map<TaskDTO[]>((await _unitOfWork.Tasks.GetAllAsync()).Where(t => t.Performer.Id == userId && t.Name.Length < 45));
        }

    }
}
