﻿using Microsoft.Extensions.Configuration;
using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProjectServer.Services
{
    public class FileService : IFileService
    {
        private IConfiguration _configuration;

        public FileService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<IEnumerable<MessageRecord>> GetMessageRecordsAsync()
        {
            string filePath = _configuration.GetSection("LogFile").Value;
            if (!File.Exists(filePath)) return new List<MessageRecord>();
            using (StreamReader sr = new StreamReader(filePath))
            {
                var jsonString = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<IEnumerable<MessageRecord>>(jsonString);
            }
        }
    }
}
