﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.Interfaces;
using ProjectShared.Entities;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private IFileService _fileService;
        public MessagesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MessageRecord>>> GetAllRecords()
        {
            return Ok(await _fileService.GetMessageRecordsAsync());
        }
    }
}