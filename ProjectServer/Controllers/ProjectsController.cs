﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using System;
using ProjectShared.DTO;
using System.Threading.Tasks;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        private readonly IMessageService _messageService;
        public ProjectsController(IProjectsService projectsService, IMessageService messageService)
        {
            _projectsService = projectsService;
            _messageService = messageService;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await _projectsService.GetAllAsync());
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return Ok(await _projectsService.GetAsync(id));
        }

        // POST: api/Projects
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectDTO value)
        {
            await _projectsService.CreateAsync(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public async Task<ActionResult> Post([FromBody] IEnumerable<ProjectDTO> values)
        {
            await _projectsService.CreateManyAsync(values);
            return Ok();
        }

        // PUT: api/Projects/5
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProjectDTO value)
        {
            await _projectsService.UpdateAsync(value);
            return Ok();
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _projectsService.DeleteAsync(id);
            return Ok();
        }

        // GET: api/Projects/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public async Task<ActionResult<ProjectStatsDTO>> GetStats(int id)
        {
            return Ok(await _projectsService.GetProjectStats(id));
        }
    }
}