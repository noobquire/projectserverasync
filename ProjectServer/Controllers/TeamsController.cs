﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Services;
using ProjectServer.Interfaces;
using System.Threading.Tasks;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITeamsService _teamsService;
        private readonly IQueueService _queueService;

        public TeamsController(IUnitOfWork unitOfWork, ITeamsService teamsService, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _teamsService = teamsService;
            _queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await _teamsService.GetAllAsync());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return Ok(await _teamsService.GetAsync(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeamDTO value)
        {
            await _teamsService.CreateAsync(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public async Task<ActionResult> Post([FromBody] IEnumerable<TeamDTO> values)
        {
            await _teamsService.CreateManyAsync(values);
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TeamDTO value)
        {
            await _teamsService.UpdateAsync(value);
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _teamsService.DeleteAsync(id);
            return Ok();
        }

        [HttpGet]
        [Route("WithUsersOlderThan12")]
        public async Task<ActionResult<IEnumerable<UsersInTeamDTO>>> GetTeamsWithUsersOlderThan12()
        {
            return Ok(await _teamsService.GetTeamsWithUsersOlderThan12Async());
        }
    }
}