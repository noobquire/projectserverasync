﻿using AutoMapper;
using ProjectShared.DTO;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<ProjectShared.Entities.Task, TaskDTO>()
                .ForMember(dest => dest.ProjectId, opts => opts.MapFrom(src => src.Project.Id))
                .ForMember(dest => dest.PerformerId, opts => opts.MapFrom(src => src.Performer.Id))
                .ForMember(dest => dest.FinishedAt, opts => opts.MapFrom(src => src.CompletedAt));
            //CreateMap<int, ProjectShared.Entities.Task>().ConvertUsing<EntityConverter<ProjectShared.Entities.Task>>();
            CreateMap<TaskDTO, ProjectShared.Entities.Task>()
                .ForMember(dest => dest.CompletedAt, opts => opts.MapFrom(src => src.FinishedAt))
                //.ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForPath(dest => dest.Project.Id, opts => opts.MapFrom(src => src.ProjectId))
                .ForPath(dest => dest.Performer.Id, opts => opts.MapFrom(src => src.PerformerId));
        }
    }
}
