﻿using AutoMapper;
using ProjectServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.MappingProfiles
{

    public class EntityConverter<T> : ITypeConverter<int, T> where T : class
    {
        private readonly IRepository<T> _repository;
        public EntityConverter(IRepository<T> repository)
        {
            _repository = repository;
        }

        public T Convert(int source, T destination, ResolutionContext context)
        {
            return _repository.GetAsync(source).Result;
        }
    }

}
