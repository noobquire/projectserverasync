﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectShared.Entities;
using ProjectShared.DTO;
using ProjectServer.DAL;

namespace ProjectServer.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(dest => dest.AuthorId, opts => opts.MapFrom(src => src.Author.Id))
                .ForMember(dest => dest.TeamId, opts => opts.MapFrom(src => src.Team.Id))
                .ForMember(dest => dest.CreatedAt, opts => opts.MapFrom(src => src.StartedAt));
            
            //CreateMap<int, Project>().ConvertUsing<EntityConverter<Project>>();
            CreateMap<ProjectDTO, Project>()
                .ForMember(dest => dest.StartedAt, opts => opts.MapFrom(src => src.CreatedAt))
                //.ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForPath(dest => dest.Author.Id, opts => opts.MapFrom(src => src.AuthorId))
                .ForPath(dest => dest.Team.Id, opts => opts.MapFrom(src => src.TeamId));
        }
    }
}
